"""uFaber URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
import taskManager.views

urlpatterns = [
    #path('admin/', admin.site.urls),
    path('login/', taskManager.views.login),
    path('logout/', taskManager.views.logout),
    path('createuser/', taskManager.views.createuser),
    path('checkunique/', taskManager.views.checkunique),
    path('createproject/', taskManager.views.createproject),
    path('createtask/', taskManager.views.createtask),
    path('createsubtask/', taskManager.views.createsubtask),
    path('editwork/', taskManager.views.editwork),
    path('updatework/', taskManager.views.updatework),
    path('assigntask/', taskManager.views.assigntask),
    path('deletework/', taskManager.views.deletework),
    path('uploadimage/', taskManager.views.uploadimage),
    path('viewteam/', taskManager.views.viewteam),
    path('viewlist/', taskManager.views.viewlist),
    path('viewdetail/', taskManager.views.viewdetail),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
