# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .utility import loggerHandling
from django.db import connection

logger = loggerHandling('logs/project.log')

from .models import sessionMaster, loginMaster, adminMaster, projectMaster, taskMaster, subTaskMaster
from .dataViewHolder import getLoginObjectModel, getAdminObjectModel, getProjectObjectModel, getTaskObjectModel, getProjectStartDate, getTaskStartDate, getSubTaskStartDate, getProjectItemIdList

##############################################################
##############################################################
##############################################################
################FOR EVALUATION PURPOSE ONLY###################
#####################Author:HARSHVARDHAN######################
###################DATA MANIPULATION LAYER####################
##############################################################
##############################################################
##############################################################

def insertSessionInformation(sessionInfoPassed):
    try:
        sessionInfo = sessionMaster()
        sessionInfo.basicAuthenticate = sessionInfoPassed.basicKey
        sessionInfo.sessionInfo = sessionInfoPassed.sessionKey
        sessionInfo.createdOn = sessionInfoPassed.createdOn
        sessionInfo.userId = getLoginObjectModel(sessionInfoPassed.userId)
        sessionInfo.isActive = sessionInfoPassed.isActive
        sessionInfo.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def deActivateASession(sessionInfoPassed):
    try:
        sessionInfo = sessionMaster.objects.get(basicAuthenticate__exact=sessionInfoPassed.basicKey)
        sessionInfo.isActive = 0
        sessionInfo.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def inserLoginInformation(userAccountInfo):
    try:
        loginInfo = loginMaster()
        loginInfo.userName = userAccountInfo.userName
        loginInfo.password = userAccountInfo.password
        loginInfo.isActive = 1
        loginInfo.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def insertPersonalDetaild(userAccountInfo):
    try:
        adminInfo = adminMaster()
        adminInfo.userId = getLoginObjectModel(userAccountInfo.userId)
        adminInfo.adminName = userAccountInfo.adminName
        adminInfo.number = userAccountInfo.number
        adminInfo.email = userAccountInfo.email
        adminInfo.isManager = userAccountInfo.isManager
        adminInfo.isActive = 1
        adminInfo.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def insertNewProject(workingInfo):
    try:
        projectInfo = projectMaster()
        projectInfo.projectName = workingInfo.name
        projectInfo.description = workingInfo.description
        projectInfo.createdOn = workingInfo.createdOn
        projectInfo.expectedStartDate = workingInfo.expectedStartDate
        projectInfo.expectedEndDate = workingInfo.expectedEndDate
        projectInfo.expectedDuration = workingInfo.expectedDuration
        projectInfo.currentStatus = workingInfo.currentStatus
        projectInfo.createdBy = getLoginObjectModel(workingInfo.createdBy)
        projectInfo.assignedTo = getAdminObjectModel(workingInfo.assignedTo)
        projectInfo.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def insertNewTask(workingInfo):
    try:
        taskInfo = taskMaster()
        taskInfo.taskName = workingInfo.name
        taskInfo.description = workingInfo.description
        taskInfo.createdOn = workingInfo.createdOn
        taskInfo.expectedStartDate = workingInfo.expectedStartDate
        taskInfo.expectedEndDate = workingInfo.expectedEndDate
        taskInfo.expectedDuration = workingInfo.expectedDuration
        taskInfo.currentStatus = workingInfo.currentStatus
        taskInfo.createdBy = getLoginObjectModel(workingInfo.createdBy)
        taskInfo.assignedTo = getAdminObjectModel(workingInfo.assignedTo)
        taskInfo.projectId = getProjectObjectModel(workingInfo.superId)
        taskInfo.isDeleted = 0
        taskInfo.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def insertNewSubTask(workingInfo):
    try:
        suTaskInfo = subTaskMaster()
        suTaskInfo.subTaskName = workingInfo.name
        suTaskInfo.description = workingInfo.description
        suTaskInfo.createdOn = workingInfo.createdOn
        suTaskInfo.expectedStartDate = workingInfo.expectedStartDate
        suTaskInfo.expectedEndDate = workingInfo.expectedEndDate
        suTaskInfo.expectedDuration = workingInfo.expectedDuration
        suTaskInfo.currentStatus = workingInfo.currentStatus
        suTaskInfo.createdBy = getLoginObjectModel(workingInfo.createdBy)
        suTaskInfo.assignedTo = getAdminObjectModel(workingInfo.assignedTo)
        suTaskInfo.taskId = getTaskObjectModel(workingInfo.superId)
        suTaskInfo.isDeleted = 0
        suTaskInfo.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def editProjectInfo(editInfo):
    try:
        projectInfo = projectMaster.objects.get(projectId=editInfo.editId)
        projectInfo.projectName = editInfo.name
        projectInfo.description = editInfo.description
        projectInfo.lastEditDate = editInfo.editDate
        projectInfo.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def editTaskInfo(editInfo):
    try:
        taskInfo = taskMaster.objects.get(taskId=editInfo.editId)
        taskInfo.taskName = editInfo.name
        taskInfo.description = editInfo.description
        taskInfo.lastEditDate = editInfo.editDate
        taskInfo.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def editSubTaskInfo(editInfo):
    try:
        subTaskInfo = subTaskMaster.objects.get(subTaskId=editInfo.editId)
        subTaskInfo.subTaskName = editInfo.name
        subTaskInfo.description = editInfo.description
        subTaskInfo.lastEditDate = editInfo.editDate
        subTaskInfo.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def updateProjectInfo(updateInfo):
    try:
        projectInfo = projectMaster.objects.get(projectId=updateInfo.updateId)
        projectInfo.currentStatus = updateInfo.updateStaus
        if updateInfo.updateStaus=="START":
            projectInfo.actualStartDate = updateInfo.updateDate
        elif updateInfo.updateStaus=="END":
            projectInfo.actualEndDate = updateInfo.updateDate
            startDate = getProjectStartDate(updateInfo.updateId)
            actualDuration = (updateInfo.updateDate-startDate).days+1
            projectInfo.actualDuration = actualDuration
        projectInfo.save()
        if updateInfo.updateStaus=="END":
            markSubTaskAsEnd(updateInfo)
            markTaskAsEnd(updateInfo)
        return True
    except Exception as e:
        logger.exception(e)
        return False

def updateTaskInfo(updateInfo):
    try:
        taskInfo = taskMaster.objects.get(taskId=updateInfo.updateId)
        taskInfo.currentStatus = updateInfo.updateStaus
        if updateInfo.updateStaus=="START":
            taskInfo.actualStartDate = updateInfo.updateDate
        elif updateInfo.updateStaus=="END":
            taskInfo.actualEndDate = updateInfo.updateDate
            startDate = getTaskStartDate(updateInfo.updateId)
            actualDuration = (updateInfo.updateDate-startDate).days+1
            taskInfo.actualDuration = actualDuration
        taskInfo.save()
        if updateInfo.updateStaus=="END":
            updateSubTaskEndFromTaskId(updateInfo.updateId, updateInfo.updateDate)
        return True
    except Exception as e:
        logger.exception(e)
        return False

def updateSubTaskInfo(updateInfo):
    try:
        subTaskInfo = subTaskMaster.objects.get(subTaskId=updateInfo.updateId)
        subTaskInfo.currentStatus = updateInfo.updateStaus
        if updateInfo.updateStaus=="START":
            subTaskInfo.actualStartDate = updateInfo.updateDate
        elif updateInfo.updateStaus=="END":
            subTaskInfo.actualEndDate = updateInfo.updateDate
            startDate = getSubTaskStartDate(updateInfo.updateId)
            actualDuration = (updateInfo.updateDate-startDate).days+1
            subTaskInfo.actualDuration = actualDuration
        subTaskInfo.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def deleteLoginInfo(userId):
    try:
        loginInsertion = loginMaster.objects.get(userId=userId)
        loginInsertion.delete()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def markTaskAsEnd(updateInfo):
    try:
        taskInfo = taskMaster.objects.filter(projectId=updateInfo.updateId).update(currentStatus="END", actualEndDate=updateInfo.updateDate)
        return True
    except Exception as e:
        logger.exception(e)
        return False

def updateSubTaskEndFromTaskId(taskId, endDate):
    try:
        subTaskInfo = subTaskMaster.objects.filter(taskId=taskId).update(currentStatus="END", actualEndDate=endDate)
        return True
    except Exception as e:
        logger.exception(e)
        return False

def assignProjectInfo(aisgnmentInfo):
    try:
        projectInfo = projectMaster.objects.get(projectId=aisgnmentInfo.assignmentId)
        projectInfo.assignedTo = getAdminObjectModel(aisgnmentInfo.assignedTo)
        projectInfo.lastEditDate = aisgnmentInfo.assignmentDate
        projectInfo.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def updateFileUrlProject(projectId, fileName):
    try:
        projectInfo = projectMaster.objects.get(projectId=projectId)
        projectInfo.imageUrl = fileName
        projectInfo.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def assignTaskInfo(aisgnmentInfo):
    try:
        taskInfo = taskMaster.objects.get(taskId=aisgnmentInfo.assignmentId)
        taskInfo.assignedTo = getAdminObjectModel(aisgnmentInfo.assignedTo)
        taskInfo.lastEditDate = aisgnmentInfo.assignmentDate
        taskInfo.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def deleteTheTask(taskId):
    try:
        taskInfo = taskMaster.objects.get(taskId=taskId)
        taskInfo.isDeleted = 1
        taskInfo.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def deleteTheSubTask(subTaskId):
    try:
        subTaskInfo = subTaskMaster.objects.get(subTaskId=subTaskId)
        subTaskInfo.isDeleted = 1
        subTaskInfo.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def assignSubTaskInfo(aisgnmentInfo):
    try:
        subTaskInfo = subTaskMaster.objects.get(subTaskId=aisgnmentInfo.assignmentId)
        subTaskInfo.assignedTo = getAdminObjectModel(aisgnmentInfo.assignedTo)
        subTaskInfo.lastEditDate = aisgnmentInfo.assignmentDate
        subTaskInfo.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def markSubTaskAsEnd(updateInfo):
    itemIdList = getProjectItemIdList(updateInfo.updateId)
    for each in itemIdList:
        updateSubTaskEndFromTaskId(each[0], updateInfo.updateDate)
    return True
