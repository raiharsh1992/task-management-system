# -*- coding: utf-8 -*-
##############################################################
##############################################################
##############################################################
################FOR EVALUATION PURPOSE ONLY###################
#####################Author:HARSHVARDHAN######################
#################DATA CLASS AND OBJECT LAYER##################
##############################################################
##############################################################
##############################################################

class loginReuest:
    def __init__(self, userName, password):
        self.userName = userName
        self.password = password

class sessionInfo:
    def __init__(self, sessionId, basicKey, sessionKey, createdOn, userId, isActive, isModerator):
        self.sessionId = sessionId
        self.basicKey = basicKey
        self.sessionKey = sessionKey
        self.createdOn = createdOn
        self.userId = userId
        self.isActive = isActive
        self.isModerator = isModerator

class userAccount:
    def __init__(self, userName, password, userId, adminName, number, email, isManager):
        self.userName = userName
        self.password = password
        self.userId = userId
        self.adminName = adminName
        self.number = number
        self.email = email
        self.isManager = isManager

class workMaster:
    def __init__(self, name, description, createdOn, expectedStartDate, actualStartDate, expectedEndDate, actualEndDate, lastEditDate, expectedDuration, actualDuration, imageUrl, currentStatus, createdBy, assignedTo, superId, isDeleted):
        self.name = name
        self.description = description
        self.createdOn = createdOn
        self.expectedStartDate = expectedStartDate
        self.actualStartDate = actualStartDate
        self.expectedEndDate = expectedEndDate
        self.actualEndDate = actualEndDate
        self.lastEditDate = lastEditDate
        self.expectedDuration = expectedDuration
        self.actualDuration = actualDuration
        self.imageUrl = imageUrl
        self.currentStatus = currentStatus
        self.createdBy = createdBy
        self.assignedTo = assignedTo
        self.superId = superId
        self.isDeleted = isDeleted

class editMaster:
    def __init__(self, editId, name, description, editDate, contolId):
        self.editId = editId
        self.name = name
        self.description = description
        self.editDate = editDate
        self.contolId = contolId

class updateMaster:
    def __init__(self, updateId, updateStaus, updateDate, updateType, controlId):
        self.updateId = updateId
        self.updateStaus = updateStaus
        self.updateDate = updateDate
        self.updateType = updateType
        self.controlId = controlId

class assignmentMaster:
    def __init__(self, assignmentId, assigneeId, assignedTo, assignmentFor, assignmentDate):
        self.assignmentId = assignmentId
        self.assigneeId = assigneeId
        self.assignedTo = assignedTo
        self.assignmentFor = assignmentFor
        self.assignmentDate = assignmentDate
