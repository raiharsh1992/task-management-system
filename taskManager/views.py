# -*- coding: utf-8 -*-
from django.http import JsonResponse
from .utility import loggerHandling
import json

logger = loggerHandling('logs/project.log')

from .workHolder import loginUser, logginOutUser, creatingNewUser, checkingForUniqueValues, creatingNewProject, creatingNewTask, creatingNewSubTask, editingWork, updatingWork, assigningTask, deletingWork, uploadingImage, viewingTeamMember, viewingAllTheList, viewingWorkDetails

##############################################################
##############################################################
##############################################################
################FOR EVALUATION PURPOSE ONLY###################
#####################Author:HARSHVARDHAN######################
#################REQUEST VIEWING HOLDER LAYER#################
##############################################################
##############################################################
##############################################################

#Login Api Interaction Point Begins
def login(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Login in for'+str(request.body))
                response = loginUser(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Logout Api Interaction Point Begins
def logout(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Logging out user for'+str(request.body))
                response = logginOutUser(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Crate new user Api Interaction Point Begins
def createuser(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Creating user for'+str(request.body))
                response = creatingNewUser(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Check if is passed values are unique Api Interaction Point Begins
def checkunique(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Checking validation for'+str(request.body))
                response = checkingForUniqueValues(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Check if is passed values are unique Api Interaction Point Begins
def createproject(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Checking validation for'+str(request.body))
                response = creatingNewProject(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Check if is passed values are unique Api Interaction Point Begins
def createtask(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Checking validation for'+str(request.body))
                response = creatingNewTask(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Check if is passed values are unique Api Interaction Point Begins
def createsubtask(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Checking validation for'+str(request.body))
                response = creatingNewSubTask(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Check if is passed values are unique Api Interaction Point Begins
def editwork(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Checking validation for'+str(request.body))
                response = editingWork(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Check if is passed values are unique Api Interaction Point Begins
def updatework(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Checking validation for'+str(request.body))
                response = updatingWork(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Check if is passed values are unique Api Interaction Point Begins
def assigntask(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Checking validation for'+str(request.body))
                response = assigningTask(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Check if is passed values are unique Api Interaction Point Begins
def deletework(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Checking validation for'+str(request.body))
                response = deletingWork(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Check if is passed values are unique Api Interaction Point Begins
def uploadimage(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Checking validation for')
                response = uploadingImage(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Check if is passed values are unique Api Interaction Point Begins
def viewteam(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Checking validation for')
                response = viewingTeamMember(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Check if is passed values are unique Api Interaction Point Begins
def viewlist(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Checking validation for')
                response = viewingAllTheList(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Check if is passed values are unique Api Interaction Point Begins
def viewdetail(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Checking validation for')
                response = viewingWorkDetails(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response
