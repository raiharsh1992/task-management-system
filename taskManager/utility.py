import logging
from datetime import datetime
import re

##############################################################
##############################################################
##############################################################
################FOR EVALUATION PURPOSE ONLY###################
#####################Author:HARSHVARDHAN######################
######################UTILITY LAYER###########################
##############################################################
##############################################################
##############################################################

def loggerHandling(location):
    logger1 = logging.getLogger(__name__)
    logger1.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(levelname)s:%(asctime)s:%(message)s')
    file_handler = logging.FileHandler(location)
    file_handler.setFormatter(formatter)
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(formatter)
    logger1.addHandler(file_handler)
    logger1.addHandler(stream_handler)
    return logger1

def isThePasswordValid(password):
    if len(password)<8:
        return False
    elif re.search('[0-9]',password) is None:
        return False
    elif re.search('[A-Z]', password) is None:
        return False
    elif re.search('[a-z]', password) is None:
        return False
    else:
        return True
