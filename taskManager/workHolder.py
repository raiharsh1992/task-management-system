# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
from datetime import datetime, date, timedelta
from django.http import JsonResponse
import os
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.conf import settings

from .utility import loggerHandling, isThePasswordValid
from .dataViewHolder import isValidLoginRequest, isValidSessionInformation, isTheUserModerator, getValifCreationUserId, isTheUserNameUnique, isTheMobileNumberUnique, isTheEmailUnique, isValidProjectId, isValidTaskId
from .dataClasses import loginReuest, sessionInfo, userAccount, workMaster, editMaster, updateMaster, assignmentMaster
from .dataControl import getSessionInfoObject, checkIfAllowedCreation, isAllowedToCreateWork, isAllowedToCreateWorkTask, isAllowedToCreateWorkSubTask, editProjIsAllowed, editTaskIsAllowed, editSubTaskIsAllowed, updateProjIsAllowed, updateTaskIsAllowed, updateSubTaskIsAllowed, isAssignmentAllowedProject, isAssignmentAllowedTask, isAssignmentAllowedSubTask, isAllowedTaskDeletion, isAllowedSubTaskDeletion, isAllowedToUploadImage, getClienMemberList, getProjectList, getTaskList, getSubTaskList, getDetailsOfProject, gerDetailsOfTask, getDetailsOfSubTask
from .dataManipulator import insertSessionInformation, deActivateASession, inserLoginInformation, insertPersonalDetaild, deleteLoginInfo, insertNewProject, insertNewTask, insertNewSubTask, editProjectInfo, editTaskInfo, editSubTaskInfo, updateProjectInfo, updateTaskInfo, updateSubTaskInfo, assignProjectInfo, assignTaskInfo, assignSubTaskInfo, deleteTheTask, deleteTheSubTask, updateFileUrlProject

logger = loggerHandling('logs/project.log')
##############################################################
##############################################################
##############################################################
################FOR EVALUATION PURPOSE ONLY###################
#####################Author:HARSHVARDHAN######################
####################DATA WORK HOLDER LAYER####################
##############################################################
##############################################################
##############################################################

# Manages the login request for validating if the values are correct and then return the session values
def loginUser(request):
    try:
        #reading Json request
        rString = json.loads(request.body)
        loginReuestObject = loginReuest(rString['userName'], rString['password'])
        #Check if valid username password and usertype exists in the system
        if isValidLoginRequest(loginReuestObject):
            #Create a session object with the provided login details
            sessionInfoObject = getSessionInfoObject(loginReuestObject)
            #Check if the created session object has values or not
            if sessionInfoObject==0:
                response = JsonResponse({"Data":"There is some inconsitency in data processing"})
                response.status_code = 409
                return response
            else:
                #Insert session information in the database, and if success return session info or else return not able to process information in this moment
                if insertSessionInformation(sessionInfoObject):
                    response = JsonResponse({"Data":"Login successful","sessionInfo":{"basicAuth":sessionInfoObject.basicKey, "sessionKey":sessionInfoObject.sessionKey,"userId":sessionInfoObject.userId, "isModerator":sessionInfoObject.isModerator}})
                    response.status_code = 200
                    return response
                else:
                    response = JsonResponse({"Data":"Not able to login in this moment please try again later"})
                    response.status_code = 409
                    return response
        else:
            response = JsonResponse({"Data":"Invalid login request passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def logginOutUser(request):
    try:
        basicKey = request.META['HTTP_BASICAUTHENTICATE']
        sessionKey = request.META['HTTP_SESSIONKEY']
        rString = json.loads(request.body)
        sessionInfoObject = sessionInfo(None, basicKey, sessionKey, None, rString['userId'], 1, None)
        if isValidSessionInformation(sessionInfoObject):
            if deActivateASession(sessionInfoObject):
                response = JsonResponse({"Data":"User logged out"})
                response.status_code = 200
                return response
            else:
                response = JsonResponse({"Data":"Not able to perform the task, please try again later"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"Invalid session information passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def creatingNewUser(request):
    try:
        basicKey = request.META['HTTP_BASICAUTHENTICATE']
        sessionKey = request.META['HTTP_SESSIONKEY']
        rString = json.loads(request.body)
        sessionInfoObject = sessionInfo(None, basicKey, sessionKey, None, rString['userId'], 1, 0)
        if isValidSessionInformation(sessionInfoObject):
            if isTheUserModerator(sessionInfoObject.userId):
                userAccountInfo = userAccount(rString['userName'], rString['password'], 0, rString['adminName'], rString['number'], rString['email'], rString['isManager'])
                isAllowedCreation = checkIfAllowedCreation(userAccountInfo)
                if isAllowedCreation==1:
                    if inserLoginInformation(userAccountInfo):
                        userId = getValifCreationUserId(userAccountInfo)
                        if userId>0:
                            userAccountInfo.userId = userId
                            if insertPersonalDetaild(userAccountInfo):
                                response = JsonResponse({"Data":"New user created successfuly"})
                                response.status_code = 200
                                return response
                            else:
                                deleteLoginInfo(userId)
                                response = JsonResponse({"Data":"Issue while creating new user please try again later"})
                                response.status_code = 409
                                return response
                        else:
                            response = JsonResponse({"Data":"Issue while creating new user please try again later"})
                            response.status_code = 409
                            return response
                    else:
                        response = JsonResponse({"Data":"Issue while creating new user please try again later"})
                        response.status_code = 409
                        return response
                else:
                    return isAllowedCreation
            else:
                response = JsonResponse({"Data":"You are not authorized to perform the task"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"Invalid session information passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def checkingForUniqueValues(request):
    try:
        basicKey = request.META['HTTP_BASICAUTHENTICATE']
        sessionKey = request.META['HTTP_SESSIONKEY']
        rString = json.loads(request.body)
        sessionInfoObject = sessionInfo(None, basicKey, sessionKey, None, rString['userId'], 1, None)
        if isValidSessionInformation(sessionInfoObject):
            if isTheUserModerator(sessionInfoObject.userId):
                checkingFor = rString['checkingFor']
                checkValue = rString['checkValue']
                if checkingFor=="UNAME":
                    if isTheUserNameUnique(checkValue):
                        response = JsonResponse({"Data":"Unique username passed"})
                        response.status_code = 200
                        return response
                    else:
                        response = JsonResponse({"Data":"Username is already taken"})
                        response.status_code = 409
                        return response
                elif checkingFor=="PASS":
                    if isThePasswordValid(checkValue):
                        response = JsonResponse({"Data":"Valid password passed"})
                        response.status_code = 200
                        return response
                    else:
                        response = JsonResponse({"Data":"Invalid password passed"})
                        response.status_code = 409
                        return response
                elif checkingFor=="NUMBER":
                    if isTheMobileNumberUnique(checkValue):
                        response = JsonResponse({"Data":"Unique mobile number passed"})
                        response.status_code = 200
                        return response
                    else:
                        response = JsonResponse({"Data":"Mobile number already taken"})
                        response.status_code = 409
                        return response
                elif checkingFor=="EMAIL":
                    if isTheEmailUnique(checkValue):
                        response = JsonResponse({"Data":"Unique email address passed"})
                        response.status_code = 200
                        return response
                    else:
                        response = JsonResponse({"Data":"Email address already taken"})
                        response.status_code = 409
                        return response
                else:
                    response = JsonResponse({"Data":"Invalid request"})
                    response.status_code = 409
                    return response
            else:
                response = JsonResponse({"Data":"You are not authorized to perform the task"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"Invalid session information passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def creatingNewProject(request):
    try:
        basicKey = request.META['HTTP_BASICAUTHENTICATE']
        sessionKey = request.META['HTTP_SESSIONKEY']
        rString = json.loads(request.body)
        sessionInfoObject = sessionInfo(None, basicKey, sessionKey, None, rString['userId'], 1, None)
        if isValidSessionInformation(sessionInfoObject):
            if isTheUserModerator(sessionInfoObject.userId):
                createdOn = date.today()
                expectedEndDate = datetime.strptime(rString['expectedStartDate'], '%d/%m/%Y').date() + timedelta(days=rString['expectedDuration'])
                workingInfo = workMaster(rString['name'], rString['description'], createdOn, rString['expectedStartDate'], None, expectedEndDate, None, None, rString['expectedDuration'], None, None, "NEW", sessionInfoObject.userId, rString['assignedTo'], None, None)
                checkStatus = isAllowedToCreateWork(workingInfo)
                if checkStatus==0:
                    if insertNewProject(workingInfo):
                        response = JsonResponse({"Data":"New project created successfuly"})
                        response.status_code = 200
                        return response
                    else:
                        response = JsonResponse({"Data":"Issue while creating new project"})
                        response.status_code = 409
                        return response
                else:
                    return checkStatus
            else:
                response = JsonResponse({"Data":"You are not authorized to perform the task"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"Invalid session information passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def creatingNewTask(request):
    try:
        basicKey = request.META['HTTP_BASICAUTHENTICATE']
        sessionKey = request.META['HTTP_SESSIONKEY']
        rString = json.loads(request.body)
        sessionInfoObject = sessionInfo(None, basicKey, sessionKey, None, rString['userId'], 1, None)
        if isValidSessionInformation(sessionInfoObject):
            if isTheUserModerator(sessionInfoObject.userId):
                createdOn = date.today()
                expectedEndDate = datetime.strptime(rString['expectedStartDate'], '%d/%m/%Y').date() + timedelta(days=rString['expectedDuration'])
                workingInfo = workMaster(rString['name'], rString['description'], createdOn, rString['expectedStartDate'], None, expectedEndDate, None, None, rString['expectedDuration'], None, None, "NEW", sessionInfoObject.userId, rString['assignedTo'], rString['projectId'], 1)
                checkStatus = isAllowedToCreateWorkTask(workingInfo)
                if checkStatus==0:
                    if insertNewTask(workingInfo):
                        response = JsonResponse({"Data":"New task created successfuly"})
                        response.status_code = 200
                        return response
                    else:
                        response = JsonResponse({"Data":"Issue while creating new task"})
                        response.status_code = 409
                        return response
                else:
                    return checkStatus
            else:
                response = JsonResponse({"Data":"You are not authorized to perform the task"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"Invalid session information passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def creatingNewSubTask(request):
    try:
        basicKey = request.META['HTTP_BASICAUTHENTICATE']
        sessionKey = request.META['HTTP_SESSIONKEY']
        rString = json.loads(request.body)
        sessionInfoObject = sessionInfo(None, basicKey, sessionKey, None, rString['userId'], 1, None)
        if isValidSessionInformation(sessionInfoObject):
            if isTheUserModerator(sessionInfoObject.userId):
                createdOn = date.today()
                expectedEndDate = datetime.strptime(rString['expectedStartDate'], '%d/%m/%Y').date() + timedelta(days=rString['expectedDuration'])
                workingInfo = workMaster(rString['name'], rString['description'], createdOn, rString['expectedStartDate'], None, expectedEndDate, None, None, rString['expectedDuration'], None, None, "NEW", sessionInfoObject.userId, rString['assignedTo'], rString['taskId'], 1)
                checkStatus = isAllowedToCreateWorkSubTask(workingInfo)
                if checkStatus==0:
                    if insertNewSubTask(workingInfo):
                        response = JsonResponse({"Data":"New sub-task created successfuly"})
                        response.status_code = 200
                        return response
                    else:
                        response = JsonResponse({"Data":"Issue while creating new sub-task"})
                        response.status_code = 409
                        return response
                else:
                    return checkStatus
            else:
                response = JsonResponse({"Data":"You are not authorized to perform the task"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"Invalid session information passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def updatingWork(request):
    try:
        basicKey = request.META['HTTP_BASICAUTHENTICATE']
        sessionKey = request.META['HTTP_SESSIONKEY']
        rString = json.loads(request.body)
        sessionInfoObject = sessionInfo(None, basicKey, sessionKey, None, rString['userId'], 1, None)
        if isValidSessionInformation(sessionInfoObject):
            updateDate = date.today()
            updateInfo = updateMaster(rString['updateId'], rString['updateStaus'], updateDate, rString['updateType'], sessionInfoObject.userId)
            if updateInfo.updateType=="PROJ":
                information = updateProjIsAllowed(updateInfo)
                if information==0:
                    if updateProjectInfo(updateInfo):
                        response = JsonResponse({"Data":"Project updated successfuly"})
                        response.status_code = 200
                        return response
                    else:
                        response = JsonResponse({"Data":"Not able to edit project"})
                        response.status_code = 409
                        return response
                else:
                    return information
            elif updateInfo.updateType=="TASK":
                information = updateTaskIsAllowed(updateInfo)
                if information==0:
                    if updateTaskInfo(updateInfo):
                        response = JsonResponse({"Data":"Task information has been updated"})
                        response.status_code = 200
                        return response
                    else:
                        response = JsonResponse({"Data":"Issue while updating task"})
                        response.status_code = 409
                        return response
                else:
                    return information
            elif updateInfo.updateType=="SUBTASK":
                information = updateSubTaskIsAllowed(updateInfo)
                if information==0:
                    if updateSubTaskInfo(updateInfo):
                        response = JsonResponse({"Data":"Sub task information has been updated"})
                        response.status_code = 200
                        return response
                    else:
                        response = JsonResponse({"Data":"Issue while updating sub task"})
                        response.status_code = 409
                        return response
                else:
                    return information
            else:
                response = JsonResponse({"Data":"Invalid edit type selected"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"Invalid session information passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def editingWork(request):
    try:
        basicKey = request.META['HTTP_BASICAUTHENTICATE']
        sessionKey = request.META['HTTP_SESSIONKEY']
        rString = json.loads(request.body)
        sessionInfoObject = sessionInfo(None, basicKey, sessionKey, None, rString['userId'], 1, None)
        if isValidSessionInformation(sessionInfoObject):
            editDate = date.today()
            editInfo = editMaster(rString['editId'], rString['name'], rString['description'], editDate, sessionInfoObject.userId)
            editType = rString['editType']
            if editType=="PROJ":
                information = editProjIsAllowed(editInfo)
                if information==0:
                    if editProjectInfo(editInfo):
                        response = JsonResponse({"Data":"Project updated successfuly"})
                        response.status_code = 200
                        return response
                    else:
                        response = JsonResponse({"Data":"Not able to edit project"})
                        response.status_code = 409
                        return response
                else:
                    return information
            elif editType=="TASK":
                information = editTaskIsAllowed(editInfo)
                if information==0:
                    if editTaskInfo(editInfo):
                        response = JsonResponse({"Data":"Task information has been updated"})
                        response.status_code = 200
                        return response
                    else:
                        response = JsonResponse({"Data":"Issue while updating task"})
                        response.status_code = 409
                        return response
                else:
                    return information
            elif editType=="SUBTASK":
                information = editSubTaskIsAllowed(editInfo)
                if information==0:
                    if editSubTaskInfo(editInfo):
                        response = JsonResponse({"Data":"Sub task information has been updated"})
                        response.status_code = 200
                        return response
                    else:
                        response = JsonResponse({"Data":"Issue while updating sub task"})
                        response.status_code = 409
                        return response
                else:
                    return information
            else:
                response = JsonResponse({"Data":"Invalid edit type selected"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"Invalid session information passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def assigningTask(request):
    try:
        basicKey = request.META['HTTP_BASICAUTHENTICATE']
        sessionKey = request.META['HTTP_SESSIONKEY']
        rString = json.loads(request.body)
        sessionInfoObject = sessionInfo(None, basicKey, sessionKey, None, rString['userId'], 1, None)
        if isValidSessionInformation(sessionInfoObject):
            editDate = date.today()
            aisgnmentInfo = assignmentMaster(rString['assignmentId'], sessionInfoObject.userId, rString['assignedTo'], rString['assignmentFor'], editDate)
            if aisgnmentInfo.assignmentFor=="PROJ":
                information = isAssignmentAllowedProject(aisgnmentInfo)
                if information==0:
                    if assignProjectInfo(aisgnmentInfo):
                        response = JsonResponse({"Data":"Project updated successfuly"})
                        response.status_code = 200
                        return response
                    else:
                        response = JsonResponse({"Data":"Not able to edit project"})
                        response.status_code = 409
                        return response
                else:
                    return information
            elif aisgnmentInfo.assignmentFor=="TASK":
                information = isAssignmentAllowedTask(aisgnmentInfo)
                if information==0:
                    if assignTaskInfo(aisgnmentInfo):
                        response = JsonResponse({"Data":"Task information has been updated"})
                        response.status_code = 200
                        return response
                    else:
                        response = JsonResponse({"Data":"Issue while updating task"})
                        response.status_code = 409
                        return response
                else:
                    return information
            elif aisgnmentInfo.assignmentFor=="SUBTASK":
                information = isAssignmentAllowedSubTask(aisgnmentInfo)
                if information==0:
                    if assignSubTaskInfo(aisgnmentInfo):
                        response = JsonResponse({"Data":"Sub task information has been updated"})
                        response.status_code = 200
                        return response
                    else:
                        response = JsonResponse({"Data":"Issue while updating sub task"})
                        response.status_code = 409
                        return response
                else:
                    return information
            else:
                response = JsonResponse({"Data":"Invalid edit type selected"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"Invalid session information passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def deletingWork(request):
    try:
        basicKey = request.META['HTTP_BASICAUTHENTICATE']
        sessionKey = request.META['HTTP_SESSIONKEY']
        rString = json.loads(request.body)
        sessionInfoObject = sessionInfo(None, basicKey, sessionKey, None, rString['userId'], 1, None)
        if isValidSessionInformation(sessionInfoObject):
            if isTheUserModerator(sessionInfoObject.userId):
                deletionFor = rString['deletionFor']
                deletionId = rString['deletionId']
                if deletionFor=="TASK":
                    if isAllowedTaskDeletion(deletionId):
                        if deleteTheTask(deletionId):
                            response = JsonResponse({"Data":"Task deleted successfuly"})
                            response.status_code = 200
                            return response
                        else:
                            response = JsonResponse({"Data":"Issue while deleting the task"})
                            response.static = 409
                            return response
                    else:
                        response = JsonResponse({"Data":"Invalid task selected for deletion"})
                        response.status_code = 409
                        return response
                elif deletionFor=="SUBTASK":
                    if isAllowedSubTaskDeletion(deletionId):
                        if deleteTheSubTask(deletionId):
                            response = JsonResponse({"Data":"Su-Task deleted successfuly"})
                            response.status_code = 200
                            return response
                        else:
                            response = JsonResponse({"Data":"Issue while deleting the sub-task"})
                            response.static = 409
                            return response
                    else:
                        response = JsonResponse({"Data":"Invalid sub-task selected for deletion"})
                        response.status_code = 409
                        return response
                else:
                    response = JsonResponse({"Data":"Invalid deletion type selected"})
                    response.status_code = 409
                    return response
            else:
                response = JsonResponse({"Data":"You are not authorized to perform the task"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"Invalid session information passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def uploadingImage(request):
    try:
        basicKey = request.META['HTTP_BASICAUTHENTICATE']
        sessionKey = request.META['HTTP_SESSIONKEY']
        sessionInfoObject = sessionInfo(None, basicKey, sessionKey, None, request.POST['userId'], 1, None)
        if isValidSessionInformation(sessionInfoObject):
            projectId = request.POST['projectId']
            if isAllowedToUploadImage(sessionInfoObject.userId, projectId):
                my_uploaded_file = request.FILES['profileImage']
                fileName = str(projectId)+".png"
                isValidFile = 0
                if my_uploaded_file.content_type=='image/png':
                    fileName = str(projectId)+".png"
                    isValidFile = 0
                elif my_uploaded_file.content_type=='image/jpg':
                    fileName = str(projectId)+".jpg"
                    isValidFile = 0
                elif my_uploaded_file.content_type=='image/jpeg':
                    fileName = str(projectId)+".jpeg"
                    isValidFile = 0
                else:
                    fileName = str(projectId)+".png"
                    isValidFile = 1
                if isValidFile==1:
                    response = JsonResponse({"Data":"Invalid file type selected"})
                    response.status_code = 409
                    return response
                else:
                    filePath = "media/"+fileName
                    if os.path.exists(filePath):
                        os.remove(filePath)
                    path = default_storage.save(fileName, ContentFile(my_uploaded_file.read()))
                    tmp_file = os.path.join(settings.MEDIA_ROOT, path)
                    if updateFileUrlProject(projectId, fileName):
                        response = JsonResponse({"Data":"File uploaded successfuly"})
                        response.status_code = 200
                        return response
                    else:
                        response = JsonResponse({"Data":"Issue occured while uploading the file"})
                        response.status_code = 409
                        return response
            else:
                response = JsonResponse({"Data":"Not allowed to upload images"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"Invalid session information passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def viewingTeamMember(request):
    try:
        basicKey = request.META['HTTP_BASICAUTHENTICATE']
        sessionKey = request.META['HTTP_SESSIONKEY']
        rString = json.loads(request.body)
        sessionInfoObject = sessionInfo(None, basicKey, sessionKey, None, rString['userId'], 1, None)
        if isValidSessionInformation(sessionInfoObject):
            response = getClienMemberList()
            return response
        else:
            response = JsonResponse({"Data":"Invalid session information passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def viewingAllTheList(request):
    try:
        basicKey = request.META['HTTP_BASICAUTHENTICATE']
        sessionKey = request.META['HTTP_SESSIONKEY']
        rString = json.loads(request.body)
        sessionInfoObject = sessionInfo(None, basicKey, sessionKey, None, rString['userId'], 1, None)
        if isValidSessionInformation(sessionInfoObject):
            requestFor = rString['requestFor']
            pageSize = rString['pageSize']
            pageCount = rString['pageCount']
            if pageCount>0:
                if pageSize>0 and pageSize<501:
                    if requestFor == "PROJ":
                        response = getProjectList(sessionInfoObject.userId, pageCount, pageSize)
                        return response
                    elif requestFor == "TASK":
                        projectId = rString['projectId']
                        if isValidProjectId(projectId):
                            response = getTaskList(sessionInfoObject.userId, pageCount, pageSize, projectId)
                            return response
                        else:
                            response = JsonResponse({"Data":"Invalid project selected"})
                            response.status_code = 409
                            return response
                    elif requestFor == "SUBTASK":
                        taskId = rString['taskId']
                        if isValidTaskId(taskId):
                            response = getSubTaskList(sessionInfoObject.userId, pageCount, pageSize, taskId)
                            return response
                        else:
                            response = JsonResponse({"Data":"Invalid task selected"})
                            response.status_code = 409
                            return response
                    else:
                        response = JsonResponse({"Data":"Invalid view request"})
                        response.status_code = 409
                        return response
                else:
                    response = JsonResponse({"Data":"Page size should be greater than 0 and less than 501"})
                    response.status_code = 409
                    return response
            else:
                response = JsonResponse({"Data":"Page count cannot be greater than 0"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"Invalid session information passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def viewingWorkDetails(request):
    try:
        basicKey = request.META['HTTP_BASICAUTHENTICATE']
        sessionKey = request.META['HTTP_SESSIONKEY']
        rString = json.loads(request.body)
        sessionInfoObject = sessionInfo(None, basicKey, sessionKey, None, rString['userId'], 1, None)
        if isValidSessionInformation(sessionInfoObject):
            requestFor = rString['requestFor']
            workId = rString['requestId']
            if requestFor=="PROJ":
                response = getDetailsOfProject(workId, sessionInfoObject.userId)
                return response
            elif requestFor=="TASK":
                response = gerDetailsOfTask(workId, sessionInfoObject.userId)
                return response
            elif requestFor=="SUBTASK":
                response = getDetailsOfSubTask(workId, sessionInfoObject.userId)
                return response
            else:
                response = JsonResponse({"Data":"Invalid request type"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"Invalid session information passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response
