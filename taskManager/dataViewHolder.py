# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db.models import Q
from .models import loginMaster, sessionMaster, adminMaster, projectMaster, taskMaster, subTaskMaster

##############################################################
##############################################################
##############################################################
################FOR EVALUATION PURPOSE ONLY###################
#####################Author:HARSHVARDHAN######################
##################DATA VIEWING HOLDER LAYER###################
##############################################################
##############################################################

#Select single units held here
def getUserIdFromLoginInfo(loginRequest):
    loginInfo = loginMaster.objects.values_list("userId").filter(userName__exact=loginRequest.userName).filter(password__exact=loginRequest.password).filter(isActive=1)
    return loginInfo[0][0]

def getValifCreationUserId(userAccountInfo):
    loginInfo = loginMaster.objects.values_list("userId").filter(userName__exact=userAccountInfo.userName).filter(password__exact=userAccountInfo.password).filter(isActive=1)
    return loginInfo[0][0]

def getUserAdminId(userId):
    adminInfo = adminMaster.objects.values_list("adminId").filter(userId=userId)
    return adminInfo[0][0]

def getUserAdminName(userId):
    adminInfo = adminMaster.objects.values_list("adminName").filter(userId=userId)
    return adminInfo[0][0]

def getAdminAdminName(adminId):
    adminInfo = adminMaster.objects.values_list("adminName").filter(adminId=adminId)
    return adminInfo[0][0]

def getProjectAssignedTo(projectId):
    projectInfo = projectMaster.objects.values_list("assignedTo").filter(projectId=projectId)
    return projectInfo[0][0]

def getTaskAssignedTo(taskId):
    taskInfo = taskMaster.objects.values_list("assignedTo").filter(taskId=taskId)
    return taskInfo[0][0]

def getSubTaskAssignedTo(subTaskId):
    subTaskInfo = subTaskMaster.objects.values_list("assignedTo").filter(subTaskId=subTaskId)
    return subTaskInfo[0][0]

def getProjectStatus(projectId):
    projectInfo = projectMaster.objects.values_list("currentStatus").filter(projectId=projectId)
    return projectInfo[0][0]

def getTaskStatus(taskId):
    taskInfo = taskMaster.objects.values_list("currentStatus").filter(taskId=taskId)
    return taskInfo[0][0]

def getSubTaskStatus(subTaskId):
    subTaskInfo = subTaskMaster.objects.values_list("currentStatus").filter(subTaskId=subTaskId)
    return subTaskInfo[0][0]

def getProjectStartDate(projectId):
    projectInfo = projectMaster.objects.values_list("actualStartDate").filter(projectId=projectId)
    return projectInfo[0][0]

def getTaskStartDate(taskId):
    taskInfo = taskMaster.objects.values_list("actualStartDate").filter(taskId=taskId)
    return taskInfo[0][0]

def getSubTaskStartDate(subTaskId):
    subTaskInfo = subTaskMaster.objects.values_list("actualStartDate").filter(subTaskId=subTaskId)
    return subTaskInfo[0][0]

def getProjectItemIdList(projectId):
    taskInfo = taskMaster.objects.values_list("taskId").filter(projectId=projectId)
    return taskInfo

#Get Counts manager held here
def isValidLoginRequest(loginRequest):
    #Validating if one and only one record with provide username, password and usertype exist in login master
    loginInfo = loginMaster.objects.values_list().filter(userName__exact=loginRequest.userName).filter(password__exact=loginRequest.password).filter(isActive=1).count()
    if loginInfo==1:
        return True
    else:
        return False

def isValidSessionInformation(sessionInfo):
    sessionInfoCount = sessionMaster.objects.values_list().filter(basicAuthenticate__exact=sessionInfo.basicKey).filter(sessionInfo__exact=sessionInfo.sessionKey).filter(userId=sessionInfo.userId).filter(isActive=1).count()
    if sessionInfoCount==1:
        return True
    else:
        return False

def isTheUserModerator(userId):
    adminInfo = adminMaster.objects.values_list().filter(isManager=1).filter(userId=userId).filter(isActive=1).count()
    if adminInfo==1:
        return True
    else:
        return False

def validSessionBasicAut(basicKey):
    sessionInfo = sessionMaster.objects.values_list().filter(basicAuthenticate__exact=basicKey).count()
    return sessionInfo

def validSessionSessionKey(sessionKey):
    sessionInfo = sessionMaster.objects.values_list().filter(sessionInfo__exact=sessionKey).count()
    return sessionInfo

def isTheUserNameUnique(userName):
    loginInfoCount = loginMaster.objects.values_list().filter(userName__exact=userName).count()
    if loginInfoCount==0:
        return True
    else:
        return False

def isTheMobileNumberUnique(number):
    adminInfoCount = adminMaster.objects.values_list().filter(number__exact=number).count()
    if adminInfoCount==0:
        return True
    else:
        return False

def isTheEmailUnique(email):
    adminInfoCount = adminMaster.objects.values_list().filter(email__exact=email).count()
    if adminInfoCount==0:
        return True
    else:
        return False

def isValidAdminId(adminId):
    adminInfoCount = adminMaster.objects.values_list().filter(adminId=adminId).count()
    if adminInfoCount==1:
        return True
    else:
        return False

def isValidProjectId(projectId):
    projectInfoCount = projectMaster.objects.values_list().filter(projectId=projectId).count()
    if projectInfoCount==1:
        return True
    else:
        return False

def isValidTaskId(taskId):
    taskInfoCount = taskMaster.objects.values_list().filter(taskId=taskId).filter(isDeleted=0).count()
    if taskInfoCount==1:
        return True
    else:
        return False

def isValidSubTaskId(subTaskId):
    subTaskInfoCount = subTaskMaster.objects.values_list().filter(subTaskId=subTaskId).filter(isDeleted=0).count()
    if subTaskInfoCount==1:
        return True
    else:
        return False

def isValidTaskStartDate(projectId, expectedStartDate):
    projectInfoCount = projectMaster.objects.values_list().filter(projectId=projectId).filter(expectedStartDate__lte=expectedStartDate).count()
    if projectInfoCount==1:
        return True
    else:
        return False

def isValidSubTaskStartDate(taskId, expectedStartDate):
    taskInfoCount = taskMaster.objects.values_list().filter(taskId=taskId).filter(expectedStartDate__lte=expectedStartDate).count()
    if taskInfoCount==1:
        return True
    else:
        return False

def isTheProjectAssignedToUserId(userId, projectId):
    adminId = getProjectAssignedTo(projectId)
    useAdminId = getUserAdminId(userId)
    if adminId==useAdminId:
        return True
    else:
        return False

def isTheTaskAssignedToUserId(userId, taskId):
    adminId = getTaskAssignedTo(taskId)
    useAdminId = getUserAdminId(userId)
    if adminId==useAdminId:
        return True
    else:
        return False

def isTheSubTaskAssignedToUserId(userId, taskId):
    adminId = getSubTaskAssignedTo(taskId)
    useAdminId = getUserAdminId(userId)
    if adminId==useAdminId:
        return True
    else:
        return False

#Gettin an object instance from db model
def getLoginObjectModel(userId):
    loginInfo = loginMaster.objects.get(userId=userId)
    return loginInfo

def getAdminObjectModel(adminId):
    adminInfo = adminMaster.objects.get(adminId=adminId)
    return adminInfo

def getProjectObjectModel(projectId):
    projectInfo = projectMaster.objects.get(projectId=projectId)
    return projectInfo

def getTaskObjectModel(taskId):
    taskInfo = taskMaster.objects.get(taskId=taskId)
    return taskInfo

def getAllUserInfoList():
    adminInfo = adminMaster.objects.values_list().filter(isActive=1)
    return adminInfo

def totalProjectCount():
    projectInfoCount = projectMaster.objects.values_list().count()
    return projectInfoCount

def totalTaskCount(projectId):
    taskInfoCount = taskMaster.objects.values_list().filter(isDeleted=0).filter(projectId=projectId).count()
    return taskInfoCount

def totalSubTaskCount(taskId):
    subTaskInfoCount = subTaskMaster.objects.values_list().filter(isDeleted=0).filter(taskId=taskId).count()
    return subTaskInfoCount

def getProjectListValues(pageCount, pageSize):
    pageCount = pageCount-1
    offset = pageCount*pageSize
    projectInfo = projectMaster.objects.values_list("projectId", "projectName", "description", "currentStatus", "createdBy", "assignedTo").order_by('-projectId')[offset:offset+pageSize]
    return projectInfo

def getTaskListValues(pageCount, pageSize, projectId):
    pageCount = pageCount-1
    offset = pageCount*pageSize
    taskInfo = taskMaster.objects.values_list("taskId", "taskName", "description", "currentStatus", "createdBy", "assignedTo").filter(isDeleted=0).filter(projectId=projectId).order_by('-taskId')[offset:offset+pageSize]
    return taskInfo

def getSubTaskListValues(pageCount, pageSize, taskId):
    pageCount = pageCount-1
    offset = pageCount*pageSize
    subTaskInfo = subTaskMaster.objects.values_list("subTaskId", "subTaskName", "description", "currentStatus", "createdBy", "assignedTo").filter(isDeleted=0).filter(taskId=taskId).order_by('-subTaskId')[offset:offset+pageSize]
    return subTaskInfo

def gettingTheDetailsProject(projectId):
    projectInfo = projectMaster.objects.values_list("projectId","projectName","description","createdOn","expectedStartDate","actualStartDate","expectedEndDate","actualEndDate","lastEditDate","expectedDuration","actualDuration","imageUrl","currentStatus","createdBy","assignedTo").filter(projectId=projectId)
    return projectInfo[0]

def gettingTheDetailsTask(taskId):
    taskInfo = taskMaster.objects.values_list("taskId","taskName","description","createdOn","expectedStartDate","actualStartDate","expectedEndDate","actualEndDate","lastEditDate","expectedDuration","actualDuration","currentStatus","createdBy","assignedTo").filter(taskId=taskId)
    return taskInfo[0]

def gettingTheDetailsSubTask(subTaskId):
    subTaskInfo = subTaskMaster.objects.values_list("subTaskId","subTaskName","description","createdOn","expectedStartDate","actualStartDate","expectedEndDate","actualEndDate","lastEditDate","expectedDuration","actualDuration","currentStatus","createdBy","assignedTo").filter(subTaskId=subTaskId)
    return subTaskInfo[0]
