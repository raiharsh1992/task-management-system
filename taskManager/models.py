from django.db import models

# Create your models here.
class loginMaster(models.Model):
    userId = models.AutoField(primary_key=True)
    userName = models.CharField(unique=True, blank=False, null=False, max_length=64)
    password = models.CharField(unique=False, blank=False, null=False, max_length=32)
    isActive = models.BooleanField()
    def __str__(self):
        return self.name

class adminMaster(models.Model):
    adminId = models.AutoField(primary_key=True)
    userId = models.ForeignKey(loginMaster, on_delete=models.PROTECT)
    adminName = models.CharField(max_length=255, blank=False, null=False, unique=False)
    number = models.CharField(max_length=12, blank=False, null=False, unique=True)
    email = models.CharField(max_length=255, blank=False, null=False, unique=True)
    isManager = models.BooleanField()
    isActive = models.BooleanField()
    def __str__(self):
        return self.name

class sessionMaster(models.Model):
    sessionId = models.AutoField(primary_key=True)
    basicAuthenticate = models.CharField(max_length=128, blank=False, null=False, unique=True)
    sessionInfo = models.CharField(max_length=128, blank=False, null=False, unique=True)
    createdOn = models.DateTimeField()
    userId = models.ForeignKey(loginMaster, on_delete=models.PROTECT)
    isActive = models.BooleanField()
    def __str__(self):
        return self.name

class projectMaster(models.Model):
    projectId = models.AutoField(primary_key=True)
    projectName = models.CharField(max_length=255, blank=False, unique=False, null=False)
    description = models.TextField(blank=False, null=False, unique=False)
    createdOn = models.DateField()
    expectedStartDate = models.DateField()
    actualStartDate = models.DateField(null=True)
    expectedEndDate = models.DateField()
    actualEndDate = models.DateField(null=True)
    lastEditDate = models.DateField(null=True)
    expectedDuration = models.IntegerField()
    actualDuration = models.IntegerField(null=True)
    imageUrl = models.CharField(max_length=255, blank=False, unique=True, null=True)
    currentStatus = models.CharField(max_length=255, blank=False, unique=False, null=False)
    createdBy = models.ForeignKey(loginMaster,  on_delete=models.PROTECT)
    assignedTo = models.ForeignKey(adminMaster,  on_delete=models.PROTECT)
    def __str__(self):
        return self.name

class taskMaster(models.Model):
    taskId = models.AutoField(primary_key=True)
    taskName = models.CharField(max_length=255, blank=False, unique=False, null=False)
    description = models.TextField(blank=False, null=False, unique=False)
    createdOn = models.DateField()
    expectedStartDate = models.DateField()
    actualStartDate = models.DateField(null=True)
    expectedEndDate = models.DateField()
    actualEndDate = models.DateField(null=True)
    lastEditDate = models.DateField(null=True)
    expectedDuration = models.IntegerField()
    actualDuration = models.IntegerField(null=True)
    currentStatus = models.CharField(max_length=255, blank=False, unique=False, null=False)
    createdBy = models.ForeignKey(loginMaster,  on_delete=models.PROTECT)
    assignedTo = models.ForeignKey(adminMaster,  on_delete=models.PROTECT)
    projectId = models.ForeignKey(projectMaster,  on_delete=models.PROTECT)
    isDeleted = models.BooleanField()
    def __str__(self):
        return self.name

class subTaskMaster(models.Model):
    subTaskId = models.AutoField(primary_key=True)
    subTaskName = models.CharField(max_length=255, blank=False, unique=False, null=False)
    description = models.TextField(blank=False, null=False, unique=False)
    createdOn = models.DateField()
    expectedStartDate = models.DateField()
    actualStartDate = models.DateField(null=True)
    expectedEndDate = models.DateField()
    actualEndDate = models.DateField(null=True)
    lastEditDate = models.DateField(null=True)
    expectedDuration = models.IntegerField()
    actualDuration = models.IntegerField(null=True)
    currentStatus = models.CharField(max_length=255, blank=False, unique=False, null=False)
    createdBy = models.ForeignKey(loginMaster,  on_delete=models.PROTECT)
    assignedTo = models.ForeignKey(adminMaster,  on_delete=models.PROTECT)
    taskId = models.ForeignKey(taskMaster,  on_delete=models.PROTECT)
    isDeleted = models.BooleanField()
    def __str__(self):
        return self.name
