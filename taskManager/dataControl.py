# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import JsonResponse
import uuid
from datetime import datetime, date

from .utility import isThePasswordValid
from .dataClasses import sessionInfo
from .dataViewHolder import getUserIdFromLoginInfo, validSessionBasicAut, validSessionSessionKey, isTheUserNameUnique, isTheMobileNumberUnique, isTheEmailUnique, isTheUserModerator, isValidAdminId, isValidProjectId, isValidTaskStartDate, isValidTaskId, isValidSubTaskStartDate, isTheProjectAssignedToUserId, isTheTaskAssignedToUserId, isTheSubTaskAssignedToUserId, isValidSubTaskId, getProjectItemIdList, getProjectStatus, getTaskStatus, getSubTaskStatus, getAllUserInfoList, getUserAdminId, getProjectListValues, totalProjectCount, getUserAdminName, getAdminAdminName, getTaskListValues, totalTaskCount, getSubTaskListValues, totalSubTaskCount, gettingTheDetailsProject, gettingTheDetailsTask, gettingTheDetailsSubTask

##############################################################
##############################################################
##############################################################
################FOR EVALUATION PURPOSE ONLY###################
#####################Author:HARSHVARDHAN######################
##################DATA CONTROL HOLDER LAYER###################
##############################################################
##############################################################
##############################################################

def getSessionInfoObject(loginRequest):
    userId = getUserIdFromLoginInfo(loginRequest)
    if userId ==0:
        return userId
    else:
        basicKey = uuid.uuid4().hex
        if validSessionBasicAut(basicKey)!=0:
            while validSessionBasicAut(basicKey)!=0:
                basicKey = uuid.uuid4().hex
        sessionKey = uuid.uuid4().hex
        if validSessionSessionKey(sessionKey)!=0:
            while validSessionSessionKey(sessionKey)!=0:
                sessionKey = uuid.uuid4().hex
        createdOnDate =  datetime.now()
        isModerator = 0
        if isTheUserModerator(userId):
            isModerator = 1
        sessionInformation = sessionInfo(0, basicKey, sessionKey, createdOnDate, userId, 1, isModerator)
        print(sessionInformation.isModerator)
        return sessionInformation

def checkIfAllowedCreation(userAccountInfo):
    if isTheUserNameUnique(userAccountInfo.userName):
        if isThePasswordValid(userAccountInfo.password):
            if isTheMobileNumberUnique(userAccountInfo.number):
                if isTheEmailUnique(userAccountInfo.email):
                    if userAccountInfo.isManager==1 or userAccountInfo.isManager==0:
                        return 1
                    else:
                        response = JsonResponse({"Data":"Invalid user role selected"})
                        response.status_code = 409
                        return response
                else:
                    response = JsonResponse({"Data":"A user with the email address already exists"})
                    response.status_code = 409
                    return response
            else:
                response = JsonResponse({"Data":"A user with the mobile number already exists"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"Password doesnt match our ctiteria"})
            response.status_code = 409
            return response
    else:
        response = JsonResponse({"Data":"Username is already taken, please try another"})
        response.status_code = 409
        return response

def isAllowedToCreateWork(workingInfo):
    if isValidAdminId(workingInfo.assignedTo):
        workingInfo.expectedStartDate = datetime.strptime(workingInfo.expectedStartDate, '%d/%m/%Y').date()
        if workingInfo.createdOn<=workingInfo.expectedStartDate:
            return 0
        else:
            response = JsonResponse({"Data":"Invalid Start date selected"})
            response.status_code = 409
            return response
    else:
        response = JsonResponse({"Data":"Invalid user assigned to"})
        response.status_code = 409
        return response

def isAllowedToCreateWorkTask(workingInfo):
    if isValidAdminId(workingInfo.assignedTo):
        workingInfo.expectedStartDate = datetime.strptime(workingInfo.expectedStartDate, '%d/%m/%Y').date()
        if workingInfo.createdOn<=workingInfo.expectedStartDate:
            if isValidProjectId(workingInfo.superId):
                if isValidTaskStartDate(workingInfo.superId, workingInfo.expectedStartDate):
                    return 0
                else:
                    response = JsonResponse({"Data":"Task cannot begin befor project"})
                    response.status_code = 409
                    return response
            else:
                response = JsonResponse({"Data":"Invalid project selected"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"Invalid Start date selected"})
            response.status_code = 409
            return response
    else:
        response = JsonResponse({"Data":"Invalid user assigned to"})
        response.status_code = 409
        return response

def isAllowedToCreateWorkSubTask(workingInfo):
    if isValidAdminId(workingInfo.assignedTo):
        workingInfo.expectedStartDate = datetime.strptime(workingInfo.expectedStartDate, '%d/%m/%Y').date()
        if workingInfo.createdOn<=workingInfo.expectedStartDate:
            if isValidTaskId(workingInfo.superId):
                if isValidSubTaskStartDate(workingInfo.superId, workingInfo.expectedStartDate):
                    return 0
                else:
                    response = JsonResponse({"Data":"Sub-Task cannot begin befor Task"})
                    response.status_code = 409
                    return response
            else:
                response = JsonResponse({"Data":"Invalid Task selected"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"Invalid Start date selected"})
            response.status_code = 409
            return response
    else:
        response = JsonResponse({"Data":"Invalid user assigned to"})
        response.status_code = 409
        return response

def editProjIsAllowed(editInfo):
    if isValidProjectId(editInfo.editId):
        if isTheUserModerator(editInfo.contolId) or isTheProjectAssignedToUserId(editInfo.contolId, editInfo.editId):
            return 0
        else:
            response = JsonResponse({"Data":"You are not authorized to edit this project"})
            response.status_code = 409
            return response
    else:
        response = JsonResponse({"Data":"Invalid project selected"})
        response.status_code = 409
        return response

def editTaskIsAllowed(editInfo):
    if isValidTaskId(editInfo.editId):
        if isTheUserModerator(editInfo.contolId) or isTheTaskAssignedToUserId(editInfo.contolId, editInfo.editId):
            return 0
        else:
            response = JsonResponse({"Data":"You are not authorized to edit this task"})
            response.status_code = 409
            return response
    else:
        response = JsonResponse({"Data":"Invalid task selected"})
        response.status_code = 409
        return response

def editSubTaskIsAllowed(editInfo):
    if isValidSubTaskId(editInfo.editId):
        if isTheUserModerator(editInfo.contolId) or isTheSubTaskAssignedToUserId(editInfo.contolId, editInfo.editId):
            return 0
        else:
            response = JsonResponse({"Data":"You are not authorized to edit this sub-task"})
            response.status_code = 409
            return response
    else:
        response = JsonResponse({"Data":"Invalid sub-task selected"})
        response.status_code = 409
        return response

def updateProjIsAllowed(updateInfo):
    if isValidProjectId(updateInfo.updateId):
        if isTheProjectAssignedToUserId(updateInfo.controlId, updateInfo.updateId):
            projectStatus = getProjectStatus(updateInfo.updateId)
            if projectStatus=="NEW" and updateInfo.updateStaus=="START" or projectStatus=="START" and updateInfo.updateStaus=="END":
                return 0
            else:
                response = JsonResponse({"Data":"Invalid update status"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"Project is not assigned to user"})
            response.status_code = 409
            return response
    else:
        response = JsonResponse({"Data":"Invalid project selected"})
        response.status_code = 409
        return response

def updateTaskIsAllowed(updateInfo):
    if isValidTaskId(updateInfo.updateId):
        if isTheTaskAssignedToUserId(updateInfo.controlId, updateInfo.updateId):
            taskStatus = getTaskStatus(updateInfo.updateId)
            if taskStatus=="NEW" and updateInfo.updateStaus=="START" or taskStatus=="START" and updateInfo.updateStaus=="END":
                return 0
            else:
                response = JsonResponse({"Data":"Invalid update status"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"Project is not assigned to user"})
            response.status_code = 409
            return response
    else:
        response = JsonResponse({"Data":"Invalid project selected"})
        response.status_code = 409
        return response

def updateSubTaskIsAllowed(updateInfo):
    if isValidSubTaskId(updateInfo.updateId):
        if isTheSubTaskAssignedToUserId(updateInfo.controlId, updateInfo.updateId):
            taskStatus = getSubTaskStatus(updateInfo.updateId)
            if taskStatus=="NEW" and updateInfo.updateStaus=="START" or taskStatus=="START" and updateInfo.updateStaus=="END":
                return 0
            else:
                response = JsonResponse({"Data":"Invalid update status"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"Project is not assigned to user"})
            response.status_code = 409
            return response
    else:
        response = JsonResponse({"Data":"Invalid project selected"})
        response.status_code = 409
        return response

def isAssignmentAllowedProject(aisgnmentInfo):
    if isValidProjectId(aisgnmentInfo.assignmentId):
        if isTheUserModerator(aisgnmentInfo.assigneeId) or isTheProjectAssignedToUserId(aisgnmentInfo.assigneeId, aisgnmentInfo.assignmentId):
            if getProjectStatus(aisgnmentInfo.assignmentId)=="NEW":
                if isValidAdminId(aisgnmentInfo.assignedTo):
                    return 0
                else:
                    response = JsonResponse({"Data":"Invalid user selected for assignment"})
                    response.status_code = 409
                    return response
            else:
                response = JsonResponse({"Data":"The selected project cannot be assigned"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"You are not authorized to edit this project"})
            response.status_code = 409
            return response
    else:
        response = JsonResponse({"Data":"Invalid project selected"})
        response.status_code = 409
        return response

def isAssignmentAllowedTask(aisgnmentInfo):
    if isValidTaskId(aisgnmentInfo.assignmentId):
        if isTheUserModerator(aisgnmentInfo.assigneeId) or isTheTaskAssignedToUserId(aisgnmentInfo.assigneeId, aisgnmentInfo.assignmentId):
            if getTaskStatus(aisgnmentInfo.assignmentId)=="NEW":
                if isValidAdminId(aisgnmentInfo.assignedTo):
                    return 0
                else:
                    response = JsonResponse({"Data":"Invalid user selected for assignment"})
                    response.status_code = 409
                    return response
            else:
                response = JsonResponse({"Data":"The selected tas cannot be assigned"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"You are not authorized to edit this task"})
            response.status_code = 409
            return response
    else:
        response = JsonResponse({"Data":"Invalid task selected"})
        response.status_code = 409
        return response

def isAssignmentAllowedSubTask(aisgnmentInfo):
    if isValidSubTaskId(aisgnmentInfo.assignmentId):
        if isTheUserModerator(aisgnmentInfo.assigneeId) or isTheSubTaskAssignedToUserId(aisgnmentInfo.assigneeId, aisgnmentInfo.assignmentId):
            if getSubTaskStatus(aisgnmentInfo.assignmentId)=="NEW":
                if isValidAdminId(aisgnmentInfo.assignedTo):
                    return 0
                else:
                    response = JsonResponse({"Data":"Invalid user selected for assignment"})
                    response.status_code = 409
                    return response
            else:
                response = JsonResponse({"Data":"The selected sub-task cannot be assigned"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"You are not authorized to edit this sub-task"})
            response.status_code = 409
            return response
    else:
        response = JsonResponse({"Data":"Invalid sub-task selected"})
        response.status_code = 409
        return response

def isAllowedTaskDeletion(taskId):
    if isValidTaskId(taskId):
        if getTaskStatus(taskId)=="NEW":
            return True
        else:
            return False
    else:
        return False

def isAllowedSubTaskDeletion(subTaskId):
    if isValidSubTaskId(subTaskId):
        if getSubTaskStatus(subTaskId)=="NEW":
            return True
        else:
            return False
    else:
        return False

def isAllowedToUploadImage(userId, projectId):
    if isValidProjectId(projectId):
        if isTheUserModerator(userId) or isTheProjectAssignedToUserId(userId, projectId):
            return True
        else:
            return False
    else:
        print('wow')
        return False

def getClienMemberList():
    userInfoList = getAllUserInfoList()
    container = []
    for each in userInfoList:
        container.append({
            "clientId":each[0],
            "clientName":each[2],
            "clientNumber":each[3],
            "clientEmail":each[4],
            "isManager":each[5]
        })
    response = JsonResponse({"Data":container})
    response.status_code = 200
    return response

def getProjectList(userId, pageCount, pageSize):
    adminId = getUserAdminId(userId)
    projectList = getProjectListValues(pageCount, pageSize)
    totalProject = totalProjectCount()
    container = []
    for each in projectList:
        createdName = getUserAdminName(each[4])
        assignedTo = ''
        assignedToMe = 0
        if each[5] is not None:
            assignedTo = getAdminAdminName(each[5])
            if adminId==each[5]:
                assignedToMe = 1
        createdByMe = 0
        if each[4]==userId:
            createdByMe = 1
        container.append({
            "projectId":each[0],
            "projectName":each[1],
            "desc":each[2],
            "currentStatus":each[3],
            "createdBy":each[4],
            "assignedTo":each[5],
            "creatorName":createdName,
            "assignorName":assignedTo,
            "isCreatedByMe":createdByMe,
            "assignedToMe":assignedToMe
        })
    response = JsonResponse({"Data":container,"totalCount":totalProject})
    response.status_code = 200
    return response

def getTaskList(userId, pageCount, pageSize, projectId):
    adminId = getUserAdminId(userId)
    projectList = getTaskListValues(pageCount, pageSize, projectId)
    totalProject = totalTaskCount(projectId)
    container = []
    for each in projectList:
        createdName = getUserAdminName(each[4])
        assignedTo = ''
        assignedToMe = 0
        if each[5] is not None:
            assignedTo = getAdminAdminName(each[5])
            if adminId==each[5]:
                assignedToMe = 1
        createdByMe = 0
        if each[4]==userId:
            createdByMe = 1
        container.append({
            "taskId":each[0],
            "taskName":each[1],
            "desc":each[2],
            "currentStatus":each[3],
            "createdBy":each[4],
            "assignedTo":each[5],
            "creatorName":createdName,
            "assignorName":assignedTo,
            "isCreatedByMe":createdByMe,
            "assignedToMe":assignedToMe
        })
    response = JsonResponse({"Data":container,"totalCount":totalProject})
    response.status_code = 200
    return response

def getSubTaskList(userId, pageCount, pageSize, taskId):
    adminId = getUserAdminId(userId)
    projectList = getSubTaskListValues(pageCount, pageSize, taskId)
    totalProject = totalSubTaskCount(taskId)
    container = []
    for each in projectList:
        createdName = getUserAdminName(each[4])
        assignedTo = ''
        assignedToMe = 0
        if each[5] is not None:
            assignedTo = getAdminAdminName(each[5])
            if adminId==each[5]:
                assignedToMe = 1
        createdByMe = 0
        if each[4]==userId:
            createdByMe = 1
        container.append({
            "subTaskId":each[0],
            "subTaskName":each[1],
            "desc":each[2],
            "currentStatus":each[3],
            "createdBy":each[4],
            "assignedTo":each[5],
            "creatorName":createdName,
            "assignorName":assignedTo,
            "isCreatedByMe":createdByMe,
            "assignedToMe":assignedToMe
        })
    response = JsonResponse({"Data":container,"totalCount":totalProject})
    response.status_code = 200
    return response

def getDetailsOfProject(projectId, userId):
    if isValidProjectId(projectId):
        adminId = getUserAdminId(userId)
        projectDetails = gettingTheDetailsProject(projectId)
        createdName = getUserAdminName(projectDetails[13])
        assignedTo = ''
        assignedToMe = 0
        if projectDetails[14] is not None:
            assignedTo = getAdminAdminName(projectDetails[14])
            if adminId==projectDetails[14]:
                assignedToMe = 1
        createdByMe = 0
        if projectDetails[13]==userId:
            createdByMe = 1
        print(len(projectDetails))
        response = JsonResponse({"Data":{"projectId":projectDetails[0],"projectName":projectDetails[1], "description":projectDetails[2], "createdOn":projectDetails[3], "expectedStartDate":projectDetails[4], "actualStartDate":projectDetails[5], "expectedEndDate":projectDetails[6], "actualEndDate":projectDetails[7], "lastEditDate":projectDetails[8], "expectedDuration":projectDetails[9], "actualDuration":projectDetails[10], "imageUrl":projectDetails[11], "currentStatus":projectDetails[12],"createdBy":projectDetails[13], "assignedTo":projectDetails[14], "isCreatedByMe":createdByMe, "creatorName":createdName, "isAssignedToMe":assignedToMe, "assignedName": assignedTo}})
        response.status_code = 200
        return response
    else:
        response = JsonResponse({"Data":"Invalid project selected to display"})
        response.status_code = 409
        return response

def gerDetailsOfTask(taskId, userId):
    if isValidTaskId(taskId):
        adminId = getUserAdminId(userId)
        projectDetails = gettingTheDetailsTask(taskId)
        createdName = getUserAdminName(projectDetails[12])
        assignedTo = ''
        assignedToMe = 0
        if projectDetails[13] is not None:
            assignedTo = getAdminAdminName(projectDetails[13])
            if adminId==projectDetails[13]:
                assignedToMe = 1
        createdByMe = 0
        if projectDetails[12]==userId:
            createdByMe = 1
        response = JsonResponse({"Data":{"taskId":projectDetails[0],"taskName":projectDetails[1], "description":projectDetails[2], "createdOn":projectDetails[3], "expectedStartDate":projectDetails[4], "actualStartDate":projectDetails[5], "expectedEndDate":projectDetails[6], "actualEndDate":projectDetails[7], "lastEditDate":projectDetails[8], "expectedDuration":projectDetails[9], "actualDuration":projectDetails[10], "currentStatus":projectDetails[11], "createdBy":projectDetails[12], "assignedTo":projectDetails[13], "isCreatedByMe":createdByMe, "creatorName":createdName, "isAssignedToMe":assignedToMe, "assignedName": assignedTo}})
        response.status_code = 200
        return response
    else:
        response = JsonResponse({"Data":"Invalid task selected to display"})
        response.status_code = 409
        return response

def getDetailsOfSubTask(subTaskId, userId):
    if isValidSubTaskId(subTaskId):
        adminId = getUserAdminId(userId)
        projectDetails = gettingTheDetailsSubTask(subTaskId)
        print(projectDetails[10])
        createdName = getUserAdminName(projectDetails[12])
        assignedTo = ''
        assignedToMe = 0
        if projectDetails[13] is not None:
            assignedTo = getAdminAdminName(projectDetails[13])
            if adminId==projectDetails[13]:
                assignedToMe = 1
        createdByMe = 0
        if projectDetails[12]==userId:
            createdByMe = 1
        response = JsonResponse({"Data":{"taskId":projectDetails[0],"taskName":projectDetails[1], "description":projectDetails[2], "createdOn":projectDetails[3], "expectedStartDate":projectDetails[4], "actualStartDate":projectDetails[5], "expectedEndDate":projectDetails[6], "actualEndDate":projectDetails[7], "lastEditDate":projectDetails[8], "expectedDuration":projectDetails[9], "actualDuration":projectDetails[10], "currentStatus":projectDetails[11], "createdBy":projectDetails[12], "assignedTo":projectDetails[13], "isCreatedByMe":createdByMe, "creatorName":createdName, "isAssignedToMe":assignedToMe, "assignedName": assignedTo}})
        response.status_code = 200
        return response
    else:
        response = JsonResponse({"Data":"Invalid sub-task selected to display"})
        response.status_code = 409
        return response
